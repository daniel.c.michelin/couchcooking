import React, { Component } from "react";
import ReactDOM from "react-dom";

class Container extends Component {
  render() {
    return (
       <h1> CouchCooking </h1>
    );
  }
}

export default Container;

const wrapper = document.getElementById("container");
wrapper ? ReactDOM.render(<Container />, wrapper) : false;
